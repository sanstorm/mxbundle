<?php

namespace Sanstorm\MxBundle\Services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MxService
 *
 * @author Sanstorm
 */
class MxService {

    public $session;
    public $login;
    public $password;

    public function __construct($login, $password) {
        $this->login = $login;
        $this->password = $password;
        $url = "http://zakaz.mxgroup.ru/mxapi/?m=login&login=" . $this->login . "&password=" . $this->password . "&out=json";
        if ($responce = json_decode(file_get_contents($url), true)) {
            $this->session = $responce['session'];
        }
    }

    public function __destruct() {
        $url = "http://zakaz.mxgroup.ru/mxapi/?m=logout&sesion=" . $this->session;
        file_get_contents($url);
    }

    /*
     * Поиск по оему
     */

    public function getSearch($oem) {
        $url = "http://zakaz.mxgroup.ru/mxapi/?m=search&zapros=" . $oem . "&login=" . $this->login . "&password=" . $this->password . "&out=json";
        if ($responce = json_decode(file_get_contents($url), true)) {
            return $responce;
        }
        return "error";
    }

    /*
     * Получение аналогов
     */

    public function getAnalog($oem) {
        $url = "http://zakaz.mxgroup.ru/mxapi/?m=analog&zapros=" . $oem . "&login=" . $this->login . "&password=" . $this->password . "&out=json";
        if ($responce = json_decode(file_get_contents($url), true)) {
            return $responce;
        }
        return "error";
    }

    /*
     * Получаем список доступных магазинов по всем договорам
     */

    public function getStores() {
        $url = "http://zakaz.mxgroup.ru/mxapi/?m=getstores&session=" . $this->session . "&out=json";
        if ($responce = json_decode(file_get_contents($url), true)) {

            return $responce;
        }
        return "error";
    }

    /*
     * Добавление в корзину
     */

    public function cart($code, $count, $id) {
        $url = "http://zakaz.mxgroup.ru/mxapi/?session=" . $this->session . "&out=json&m=cart&code=" . $code . "&count=" . $count . "&id=" . $id;
        if ($responce = json_decode(file_get_contents($url), true)) {

            return $responce;
        }
        return "error";
    }

    /*
     * Получение содержимого корзины: getbasket
     */

    public function getBasket() {
        $url = "http://zakaz.mxgroup.ru/mxapi/?session=" . $this->session . "&out=json&m=getbasket";
        if ($responce = json_decode(file_get_contents($url), true)) {
            return $responce;
        }
        return "error";
    }

    /*
     * Очищение корзины: cleanbasket
     * Данная функция не принимает обязательных параметров и очищает корзину от товаров, есть два необязательных параметра:
     * code – удаляет из корзины товар с заданным кодом 1с
     * count – изменяет кол-во товара в корзине, применяется только вместе с параметром code
     */

    public function cleanBasket($code = null, $count = null) {

        $url = "http://zakaz.mxgroup.ru/mxapi/?session=" . $this->session . "&m=cleanbasket&out=json&code=" . $code . "&count=" . $count;
        if ($responce = json_decode(file_get_contents($url), true)) {
            return $responce;
        }
        return "error";
    }

    /*
     * Данная функция не принимает никаких параметров, проверяет еще раз, используя веб сервис 1с,
     *  остатки по всем заказанным позициям и оформляет заказ на все товары из корзины и очищает корзину:
     */

    public function sendOrder() {

        $url = "http://zakaz.mxgroup.ru/mxapi/?session=" . $this->session . "&m=sendorder&out=json";
        if ($responce = json_decode(file_get_contents($url), true)) {
            return $responce;
        }
        return "error";
    }
    
    /*
     * Очищение корзины: cleanbasket
     * Данная функция не принимает обязательных параметров и очищает корзину от товаров, есть два необязательных параметра:
     * code – удаляет из корзины товар с заданным кодом 1с
     * count – изменяет кол-во товара в корзине, применяется только вместе с параметром code
     */

    public function getStatus($ordernum = null) {

        $url = "http://zakaz.mxgroup.ru/mxapi/?session=" . $this->session . "&m=getstatus&out=json&ordernum=" . $ordernum;
        if ($responce = json_decode(file_get_contents($url), true)) {
            return $responce;
        }
        return "error";
    }
    
    
    /*
     * Данная функция не принимает никаких параметров,
     * возвращает значения баланса, просроченной дебиторской задолженности и кредит лимита.
     */

    public function balance() {

        $url = "http://zakaz.mxgroup.ru/mxapi/?session=" . $this->session . "&m=balance&out=json";
        if ($responce = json_decode(file_get_contents($url), true)) {
            return $responce;
        }
        return "error";
    }
}
